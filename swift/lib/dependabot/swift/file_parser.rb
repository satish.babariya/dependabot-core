# frozen_string_literal: true

require "dependabot/errors"
require "dependabot/dependency"
require "dependabot/shared_helpers"
require "dependabot/source"

require "dependabot/file_parsers"
require "dependabot/file_parsers/base"

# NOTE : Dependency Requirement Can be exact or (lowerbound to uppoerbound) or branch or Git revision
# Ignore Branch and Revision Dependency's

# Relevant Swift Package Manager docs can be found at:
# - https://github.com/apple/swift-package-manager/tree/master/Documentation
module Dependabot
  module Swift
    class FileParser < Dependabot::FileParsers::Base
      require "dependabot/file_parsers/base/dependency_set"

      def parse
        dependency_set = DependencySet.new
        dependency_set += manifest_dependencies
        dependency_set.dependencies
      end

      private

      def manifest_dependencies
        dependency_set = DependencySet.new

        REQUIREMENT_TYPES.each do |type|
          parsed_file(manifest).fetch(type, []).each do |details|
            
            dependency_set << Dependency.new(
              name: details.fetch("name"),
              version: version_from_declaration(details),
              package_manager: "swift",
              requirements: [{
                requirement: requirement_from_declaration(details),
                file: manifest.name,
                groups: [],
                source: source_from_declaration(details)
              }]
            )
          end
        end

        dependency_set
      end

      def version_from_declaration(details)
        # TODO
      end

      def source_from_declaration(declaration)
        # TODO
      end

      def requirement_from_declaration(declaration)
        # TODO
      end

      #  TODO : Parse File Using Exec

      def parsed_file(file)
        SharedHelpers.in_a_temporary_directory do
          #  TODO Write Temp Package File If Needed 
        
          SharedHelpers.run_helper_subprocess(
            command: "swift package dump-package",
            function: "parse",
            stderr_to_stdout: true
          )

          #TODO : Parse JSON

        end
      rescue Dependabot::SharedHelpers::HelperSubprocessFailed => e
        # TODO
      end

      def manifest
        @manifest ||= get_original_file("Package.swift")
      end

      def check_required_files
        %w(Package.swift).each do |filename|
          raise "No #{filename}!" unless get_original_file(filename)
        end
      end

      
    end
  end
end

Dependabot::FileParsers.register("swift", Dependabot::Swift::FileParser)
