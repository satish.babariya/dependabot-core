# frozen_string_literal: true

require "dependabot/file_fetchers"
require "dependabot/file_fetchers/base"

module Dependabot
  module Swift
    class FileFetcher < Dependabot::FileFetchers::Base
      def self.required_files_in?(filenames)
        filenames.include?("Package.swift")
      end

      def self.required_files_message
        "Repo must contain a Package.swift."
      end

      private

      def fetch_files
        fetched_files = []
        fetched_files << manifest if manifest

        unless manifest
          raise(
            Dependabot::DependencyFileNotFound,
            File.join(directory, "Package.swift")
          )
        end

        # Fetch the main.go file if present, as this will later identify
        # this repo as an app.
        fetched_files << main if main
        fetched_files
      end

      def manifest
        @manifest ||= fetch_file_if_present("Package.swift")
      end

    end
  end
end

Dependabot::FileFetchers.register("swift", Dependabot::Swift::FileFetcher)
